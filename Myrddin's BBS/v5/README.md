Myrddin's Global Bulletin Board, v5
===================================

After many years, the BBS has an official update which includes threaded replies, color support, an intelligent +bbnext, and other fixes/modifications.
It's been running successfully on active Penn and MUX games, as well as multiple development Rhost games.  Spot testing has been done on Tiny as well.

---
## INSTALL

1. Click on the [myrddin_bb.v5.code](https://bitbucket.org/myrddin0/myrddins-mush-code/src/master/Myrddin's%20BBS/v5/myrddin_bb.v5.code) file above.
2. Follow the simple instructions at the top of the file depending on whether or not you have an existing BBS.
3. Copy and paste the code to your game as is.
    * **WARNING:** the Potato client has problems copy/pasting large amounts of text and you'll have to copy/paste in small chunks.
        * Potato: Break up the copy/paste into 3 or 4 chunks.
        * TinyFugue: No paste issues.
        * BeipMU: No paste issues.
        * Atlantis: (not tested yet)

---
## Major Features New to v5

See [list of changes since v5](#markdown-header-change-log) below.

### +bbreply
-   Feature: new command, '+bbreply' will allow people to reply to a specific
    board message.  This resuls in an in-line reply with a new numbering scheme
    for replies as well as a visual, threaded cue in the message list that
    makes replies obvious.
-   Replies to replies will become replies to the parent (no nested replies;
    those are too unwieldy for a text based interface with limited
    real-estate).
-   Due to the new numbering scheme for replies, pre-existing messages will not
    have their message number changed (eg. if a board has 16 posts, and someone
    does a reply to post 5, the reply will be 5.1, leaving posts 6-16
    unchanged).

### Colors
-   Feature: The bbs supports optional and customizable colors
-   Toggle colors on/off with +bbconfig
-   Three different colors used for different parts of the output, each color can be configured
-   Color coded flags (Unread, Timeout warning, etc)

### Timestamp changes
-   Feature: the year now appears in bb postings
-   Feature: HH:MM:SS will also appear in specific message readings
-   Feature: better header layout to accomodate the change in date
-	 Feature: timestamp stored internally as epoch, to allow for future proofing of any changes in how we want to display the timestamp

### +bbnext
-   Feature: +bbnext will show 'next' unread message
-   Feature: '+bbnext <#>' will show 'next' unread message in board <#>

### +bbnew
-   Feature: '+bbnew <#>' will show a message listing of unread messages in a board.
-   Feature: '+bbnew' will show nice, full listing of all unread messages on all boards.

### Misc
-   Subject lines can now be up to 64 characters in length (previous max was 34)

### BUGFIX
-   '+bbread <group>' on an empty group will no longer have an awkward, truncated output.

---
# Change Log

## 5.2.2 (July 22, 2021):
### BUGFIX
-   Using pipes in the subject line would result in truncated subject lines or other confusion.
    (credit: Shangrila for pointing it out)

## 5.2.1 (May 29, 2021):
### BUGFIX
-   Some versions of Penn are very particular about argments to extract().
    This could cause problems with reading replies, etc.

## 5.2.0 (May 29, 2021):
### FEATURE
-   '+bbversion' can now detect locally modified code.  This will help
    discover incomplete installs or mangled installs. It will also help
    troubleshoot installs on games that might have modified the BBS code in
    such a way that could've introduced regressions.

## 5.1.2 (May 11, 2021):
### BUGFIX
-   '+bbread <#>/u' would give confusing error message if there were no unread messages on that board.


## 5.1.1 (Mar 31, 2021):
### FEATURE
-   '+bbnew' (no arg) will give a message listing of all unread messages on all boards.


## 5.1.0 (Mar 27, 2021):
### FEATURE
-   '+bbnew <#>' will show a message listing of unread messages in a board.
### BUGFIXES
-   +bbreply announcements now appropriately include the topic replied to
    rather than a message ID.
-   '+bbread' on games with large numbers of groups will no longer see that
    list truncated.
-  '+bbremove x/y' will no longer silently fail if x/y doesn't exist.


## 5.0.2 (July 7, 2020):
### BUGFIX
-   Fixed a regression introduced in 5.0.1 where creation of msg id lists would sometimes get borked.
    
## 5.0.1 (July 6, 2020):
### BUGFIX
-   A nearly full (>90%) would cause some commands to error out to to excess
    spaces embedding themselves in certain temp buffers.


