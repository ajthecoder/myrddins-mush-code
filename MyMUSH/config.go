package main

import (
	"io/ioutil"
	"os"

	"gopkg.in/yaml.v2"
)

// tips on yaml / go stuff
// https://dev.to/koddr/let-s-write-config-for-your-golang-web-app-on-right-way-yaml-5ggp

type YamlConfig struct {
	Port  int    `yaml:"port"`
	Name  string `yaml:"name"`
	Mongo struct {
		Host string `yaml:"host"`
		Port int    `yaml:"port"`
		User string `yaml:"user"`
		PW   string `yaml:"pw"`
		DB   string `yaml:"db"`
	}
}

var config YamlConfig

func load_config(cfile string) YamlConfig {

	//
	// load config file
	//
	yamlFile, err := ioutil.ReadFile(cfile)
	if err != nil {
		log.Criticalf("Error reading config file: %s", err)
		os.Exit(1)
	}

	err = yaml.Unmarshal(yamlFile, &config)
	if err != nil {
		log.Errorf("Error parsing config file: %s", err)
	}

	// log.Printf("Config: %+v\n", config)
	log.Info("Config:")
	log.Infof("\tName: %v", config.Name)

	if config.Port == 0 {
		config.Port = 4444
		log.Infof("\tPort: %v (default)", config.Port)
	} else {
		log.Infof("\tPort: %v", config.Port)
	}

	// set critical defaults if missing from config file
	if config.Mongo.Port == 0 {
		config.Mongo.Port = 27017
	}
	if config.Mongo.Host == "" {
		config.Mongo.Host = "localhost"
	}
	if config.Mongo.DB == "" {
		config.Mongo.DB = "MyMUSH"
	}

	log.Infof("\tMongo Host: %v", config.Mongo.Host)
	log.Infof("\tMongo Port: %v", config.Mongo.Port)
	log.Infof("\tMongo User: %v", config.Mongo.User)
	log.Infof("\tMongo DB:   %v", config.Mongo.DB)

	return config
}
