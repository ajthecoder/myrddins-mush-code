package main

import (
	"github.com/op/go-logging"
	"os"
)

var log = logging.MustGetLogger("MyMUSH")

func initLogging() {
	var logFormat = logging.MustStringFormatter(
		// `%{color}%{time:2006-01-02 15:04:05.000} %{shortfile} %{shortfunc} ▶ %{level:.4s}%{color:reset} %{message}`,
		`%{color}%{time:2006-01-02 15:04:05.000} %{shortfile:-15s} %{shortfunc:-17s} ▶ %{level:.4s}%{color:reset} %{message}`,
	)
	backend := logging.NewLogBackend(os.Stderr, "", 0)
	backendFormatter := logging.NewBackendFormatter(backend, logFormat)
	logging.SetBackend(backendFormatter)
}
