// vim:tw=9999
package main

import (
	"fmt"
	"net"
	"os"
	"strconv"
)

func netStartMainSock() net.Listener {
	var listener net.Listener

	localAddress := ":" + strconv.Itoa(config.Port)
	listener, err := net.Listen("tcp4", localAddress)
	if err != nil {
		log.Error(err)
		os.Exit(1)
	}
	log.Infof("main port listener: %v", listener.Addr())

	return listener
}

func netWrite(conn net.Conn, format string, a ...interface{}) {
	conn.Write([]byte(fmt.Sprintf(format, a...)))
}

func netWriteStr(conn net.Conn, buf string) {
	// conn.Write([]byte(buf))
	netWriteB(conn, []byte(buf))
}

func netWriteB(conn net.Conn, buf []byte) {
	conn.Write(buf)
}
