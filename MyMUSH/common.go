// vim:tw=99999:expandtab
package main

import (
	"context"
	"fmt"
	"net"
	"strconv"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	// "go.mongodb.org/mongo-driver/mongo"
	// "go.mongodb.org/mongo-driver/mongo/primitive"
)

type GameMetaData struct {
	next_dbref int
}

type COMMAND_GOCOMM int

const (
	cmd_gc_shutdown COMMAND_GOCOMM = iota
)

type goComm struct {
	cmd COMMAND_GOCOMM
}

type Player struct {
	_id      primitive.ObjectID
	Name     string
	Password string
	DBref    int
	Location int
	Desc     string
	Conn     net.Conn
	State    PLAYER_STATE
	Cmds     int
}

type Room struct {
	_id   primitive.ObjectID
	Name  string
	DBref int
	Owner int
	Desc  string
}

type Exit struct {
	_id         primitive.ObjectID
	Name        string
	DBref       int
	Owner       int
	Desc        string
	Location    int
	Destination int
}

type Thing struct {
	_id      primitive.ObjectID
	Name     string
	DBref    int
	Owner    int
	Desc     string
	Location int
}

// global maps
var players map[int]*Player
var gameData GameMetaData

func init_players_map() {
	log.Notice("iniitializing player map")
	players = make(map[int]*Player)
}

func get_next_dbref() int {
	dbref := gameData.next_dbref
	gameData.next_dbref++
	return dbref
}

func emote_room(loc int, msg string, skip int) {
	ctx := context.TODO()
	cursor, err := mongoPlayers.Find(ctx, bson.D{{"location", loc}})
	if err != nil {
		// TODO: emote to the player at least?
		log.Fatal(err)
	}

	for cursor.Next(ctx) {
		var tmpPlayer Player
		cursor.Decode(&tmpPlayer)
		if tmpPlayer.DBref != skip {
			if _, ok := players[tmpPlayer.DBref]; ok {
				tp := players[tmpPlayer.DBref]
				if tp.State == ps_LoggedIn && tp.Conn != nil {
					netWrite(players[tmpPlayer.DBref].Conn, "%v\n", msg)
				}
			}
			// log.Printf("cmd_say: #%v:%v\n", tmpPlayer.DBref, tmpPlayer.Name)
		}
	}
}

func notify_player(player *Player, msg_fmt string, f ...interface{}) {
	player.Conn.Write([]byte(fmt.Sprintf(msg_fmt, f...)))
}

// load_obj?  so we can get the object, put it in our in-memory map?
func find_obj(player *Player, target_name string) (interface{}, int) {
	if target_name == "" {
		return nil, -1
	}

	if target_name[0] == '#' {
		dbref, _ := strconv.Atoi(target_name[1:])
		obj, err := db_getobject(dbref)
		if err == nil {
			return nil, -1
		}
		switch obj.(type) {
		case Player:
			return obj, 0 // TODO: hmm
			break
		case Thing:
			break
		case Room:
			break
		case Exit:
			break
		default:
			break
		}
	}

	return nil, 0
}

func do_set(player *Player, opts *player_cmd_options) *Player {
	o := *opts
	if o.target_dbref == -1 {
		if o.target_name == "" {
			log.Noticef("do_set: no target provided. Player (%v: %v), attr (%v: %v)",
				player.Name, player.DBref, o.attr_name, o.attr_val)
			notify_player(player, "I don't see that here.")
			return player
		}
		// o.target_dbref = dbref_locate(player, o.target_name)
	}
	return player
}
