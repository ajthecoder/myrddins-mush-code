// vim:tw=9999
package main

import (
	"context"
	"errors"
	"os"
	"strconv"
	"strings"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var mongoclient *mongo.Client
var mongoDB *mongo.Database
var mongoPlayers *mongo.Collection
var mongoThings *mongo.Collection
var mongoRooms *mongo.Collection
var mongoExits *mongo.Collection
var mongoMeta *mongo.Collection

func connect_mongo(config YamlConfig) *mongo.Client {
	var creds string
	if config.Mongo.User != "" {
		creds = config.Mongo.User + ":" + config.Mongo.PW + "@"
	}

	URI := "mongodb://" + creds + config.Mongo.Host + ":" + strconv.Itoa(config.Mongo.Port)
	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI(URI))
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	mongoclient = client

	log.Infof("successful mongodb connection: %v", URI)

	return mongoclient
}

func db_set_globals(db *mongo.Database) {
	mongoDB = db
	mongoPlayers = db.Collection("players")
	mongoThings = db.Collection("things")
	mongoRooms = db.Collection("rooms")
	mongoExits = db.Collection("exits")
	mongoMeta = db.Collection("meta")
}

func init_db(mc *mongo.Client, dbname string) {
	result, err := mc.ListDatabaseNames(context.TODO(), bson.D{{"name", dbname}})
	if err != nil {
		log.Fatal(err)
	}

	for _, db := range result {
		// log.Printf("\t%v\n", db)
		if db == dbname {
			log.Infof("%v database exists", dbname)
			db_set_globals(mc.Database(dbname))
			return
		}
	}

	log.Noticef("%v database not found.  Generating fresh database.", dbname)
	db_set_globals(mc.Database(dbname))

	res, err := mongoMeta.InsertOne(context.Background(), bson.M{
		"type":       "database",
		"next_dbref": 0,
	})
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	// create room #0
	res, err = mongoRooms.InsertOne(context.Background(), bson.M{"dbref": 0,
		"name":  "Limbo",
		"owner": 1,
		"desc":  "A misty realm that seems to stretch to infinity."})
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	limbo_id := res.InsertedID

	gameData.next_dbref = 1

	// create Wizard
	wiz := Player{
		Name:     "Wizard",
		Password: "potrzebie",
		DBref:    get_next_dbref(),
		Desc:     "",
		Location: 0}
	db_AddPlayer(&wiz)

	db_UpdateDBrefs()

	log.Noticef("new %v database initialized with Limbo (%v) and Wizard (%v)", dbname, limbo_id, wiz._id)
}

func db_AddPlayer(player *Player) (*Player, error) {
	p := *player
	var err error

	res, err := mongoPlayers.InsertOne(context.Background(), bson.M{
		"dbref":    p.DBref,
		"name":     p.Name,
		"lcname":   strings.ToLower(p.Name),
		"password": p.Password,
		"desc":     "",
		"location": 0})

	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	player._id = res.InsertedID.(primitive.ObjectID)
	db_UpdateDBrefs()
	return player, nil
}

func db_SetPW(player *Player, newpw string) error {
	p := *player
	opts := options.Update().SetUpsert(false)
	// filter := bson.D{{"_id", p._id}}
	filter := bson.D{{"dbref", p.DBref}}
	update := bson.D{{"$set", bson.D{{"password", newpw}}}}

	log.Infof("attempting to change password for '%v (#%v)' (%v)", p.Name, p.DBref, p._id)
	result, err := mongoPlayers.UpdateOne(context.TODO(), filter, update, opts)
	if err != nil {
		log.Fatal(err)
		return err
	}

	if result.ModifiedCount == 1 {
		log.Infof("player '%v (#%v)' just changed password", p.Name, p.DBref)
	} else {
		log.Errorf("WARNING: Unable to change password for player '%v (#%v)'", p.Name, p.DBref)
		return errors.New("can't update document")
	}

	return nil
}

func list_dbs(mc *mongo.Client) {
	log.Info("listing mongo databases:")
	result, err := mc.ListDatabaseNames(context.TODO(), bson.D{})
	// result, err := mc.ListDatabaseNames(context.TODO(), bson.D{{"name", "eternal_mists"}})
	if err != nil {
		log.Fatal(err)
	}

	for _, db := range result {
		log.Infof("\t%v", db)
	}
}

func db_LoadMetaData() error {
	filter := bson.M{"type": "database"}
	err := mongoMeta.FindOne(context.TODO(), filter).Decode(&gameData)
	return err
}

//	filter := bson.D{{"dbref", p.DBref}}
//	update := bson.D{{"$set", bson.D{{"password", newpw}}}}

//	log.Printf("attempting to change password for '%v (#%v)' (%v)\n", p.Name, p.DBref, p._id)
//	result, err := mongoPlayers.UpdateOne(context.TODO(), filter, update, opts)

func db_UpdateDBrefs() error {
	opts := options.Update().SetUpsert(true)
	// filter := bson.D{{"_id", p._id}}
	filter := bson.D{{"type", "database"}}
	update := bson.D{{"$set", bson.D{{"next_dbref", gameData.next_dbref}}}}

	result, err := mongoMeta.UpdateOne(context.TODO(), filter, update, opts)
	if err != nil {
		log.Fatal(err)
		return err
	}

	if result.ModifiedCount != 1 {
		// this can happen if the next_dbref in the db already agrees with our in memory gameData.next_dbref
		log.Notice("INFO: no Upsert.  Probably due to next_dbref in mongo being the same as gameData.next_dbref")
		return errors.New("can't update document")
	}

	return nil
}

func db_getroom(dbref int) (Room, error) {
	var room Room

	// collection := mongoclient.Database(config.MongoDB).Collection("rooms")

	filter := bson.M{"dbref": dbref}
	err := mongoRooms.FindOne(context.TODO(), filter).Decode(&room)
	if err != nil {
		log.Noticef("invalid room specifier: %v", dbref)
	}

	return room, err
}

func db_getobject(dbref int) (interface{}, error) {
	var empty Player
	var err error

	return empty, err
}

func db_SetAttr(player *Player, attr string, val string) error {
	p := *player
	opts := options.Update().SetUpsert(false)
	// filter := bson.D{{"_id", p._id}}
	filter := bson.D{{"dbref", p.DBref}}
	update := bson.D{{"$set", bson.D{{attr, val}}}}

	log.Infof("attempting to set '%v: %v' for '%v (#%v)' (%v)", attr, val, p.Name, p.DBref, p._id)
	result, err := mongoPlayers.UpdateOne(context.TODO(), filter, update, opts)
	if err != nil {
		log.Fatal(err)
		return err
	}

	if result.ModifiedCount == 1 {
		log.Infof("player '%v (#%v)' just set '%v: %v'", p.Name, p.DBref, attr, val)
	} else {
		log.Errorf("WARNING: Unable to set '%v: %v' for player '%v (#%v)'", attr, val, p.Name, p.DBref)
		return errors.New("can't update document")
	}

	return nil
}
